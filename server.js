var http = require("http"),
    url=require("url"),
    https=require("https"),
    path=require("path"),
    fs = require('fs'),
    req=require('request'),
    formidable=require("formidable"),
    util=require("util"),
    restify=require("restify"),
    Router=require("router");
    var router = new Router();
    
    var firebase_json_url="https://radiant-heat-6795.firebaseio.com/assignment-part2.json"
    var firebase_url="https://radiant-heat-6795.firebaseio.com/assignment-part2/"

var server=http.createServer(function (request, response) {
    router( request, response, function( error ) {
        if ( !error ) {
          response.writeHead( 404 );
        } else {
          // Handle errors
          console.log( error.message, error.stack );
          response.writeHead( 400 );
        }
        response.end( 'RESTful API Server is running!' );
    });
}).listen(process.env.PORT);


//static directories
router.get('/datetimepicker/:subdir',printDatetimePicker)
router.get('/datetimepicker/build/:subdir',printDatetimePicker)
router.get('/js/:filename',printJavaScript)
router.get('/css/:filename',printCSS)
router.get('/ajax/:filename',ajaxHandler)
//static directories

//pages
router.get('/manage-event',editEventHandler)
router.get('/',indexHandler)
router.get('/',loginHandler)
//pages end

//api
router.get('/events',printEventList)
router.get('/weather',printWeather)
router.post('/event',addEvent)

router.post('/event/:key',updateEvent)
router.post('/event/delete/:key',delEvent)
//api end

// Console will print the message
console.log('Server running at http://127.0.0.1:8081/');

function printDatetimePicker(request,response){
    var subDir=request.params.subdir
    var path=""
    console.log('path'+request.url)
    console.log("sub"+subDir)
    if(request.url.indexOf('/build/')>-1){
        path='datetimepicker-master/build/'+getFilename(request.url)
        fs.readFile(path, function (err, script) {
            if (err) {
                throw err; 
            } 
            response.writeHead(200, {'Content-Type': 'text/javascript'});
            response.write(script)
            response.end()
        });
        // printJavaScript('datetimepicker-master/build/'+getFilename(request.url),request,response)
        
    }else{
        path='datetimepicker-master/'+getFilename(request.url)
        fs.readFile(path, function (err, script) {
            if (err) {
                throw err; 
            } 
            response.writeHead(200, {'Content-Type': 'text/css'});
            response.write(script)
            response.end()
        });
        // printCSS('datetimepicker-master/'+getFilename(request.url),request,response)
    }
     
}

function ajaxHandler(request,response){
    printHtml('ajax/'+request.params.filename,response)
}

function editEventHandler(request,response){
    printHtml('event.html',response)
}

function indexHandler(request,response){
    printHtml('index.html',response)
}

function loginHandler(request,response){
    printHtml('login.html',response)
}

function getFilename(path){
    var startIndex = (path.indexOf('\\') >= 0 ? path.lastIndexOf('\\') : path.lastIndexOf('/'));
    var filename = path.substring(startIndex);
    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        filename = filename.substring(1);
    }
    return filename
}

function printHtml(path,response){
    fs.readFile(path, function (err, html) {
        if (err) {
            throw err; 
        } 
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(html)
        response.end()
    }); 
}

function printCSS(request,response){
    console.log(request.params.filename)
    path='css/'+request.params.filename
        fs.readFile(path, function (err, css) {
            if (err) {
                throw err; 
            } 
            response.writeHead(200, {'Content-Type': 'text/css'});
            response.write(css)
            response.end()
    });  
}

function printJavaScript(request,response){
    console.log(request.params.filename)
    path='js/'+request.params.filename
    fs.readFile(path, function (err, script) {
        if (err) {
            throw err; 
        } 
        response.writeHead(200, {'Content-Type': 'text/javascript'});
        response.write(script)
        response.end()
    }); 
}

function addEvent(request,response){
    var fields = {};
    var form = new formidable.IncomingForm();
    form.on('field', function (field, value) {
        console.log(field);
        console.log(value);
        fields[field] = value;
    });

    form.on('end', function () {
        console.log(fields)
        req.post({url:firebase_json_url,json:fields},function(err,res,body){
            if (err) {
                return console.error('post failed:', err);
              }
        
          console.log('Post successful!  Server responded with:', body);
        })
        response.writeHead(301,
          {Location: '/manage-event'}
        );
        response.end()
    });
    form.parse(request);
}

function delEvent(request,response){
    var eventKey=request.params.key
    console.log('deleting event')
    var form = new formidable.IncomingForm();
    form.on('field', function (field, value) {
    });

    form.on('end', function () {
        req.delete({url:firebase_url+request.params.key+".json"},function(err,res,body){
            if (err) {
                return console.error('post failed:', err);
              }
        
          console.log('Post successful!  Server responded with:', body);
        });
        response.writeHead(301,
              {Location: '/manage-event'}
        );
        response.end()
    })
    form.parse(request);
}

function updateEvent(request,response){
    var fields = {};
    var form = new formidable.IncomingForm();
    form.on('field', function (field, value) {
        console.log(field);
        console.log(value);
        fields[field] = value;
    });

    form.on('end', function () {
        console.log(fields)
        req.patch({url:firebase_url+request.params.key+".json",json:fields},function(err,res,body){
            if (err) {
                return console.error('post failed:', err);
              }
        
          console.log('Post successful!  Server responded with:', body);
        });
        response.writeHead(301,
              {Location: '/manage-event'}
        );
        response.end()
    });
    form.parse(request);
}

function printEventList(request,response){
    var api_request = https.get(firebase_json_url, function (api_response) {
        var buffer = "", 
            data,
            route;
    
        api_response.on("data", function (chunk) {
            buffer += chunk;
        }); 
        
        api_response.on("end", function (err) {
            // finished transferring data
            // dump the raw data
            console.log(buffer);
            console.log("\n");
            data = JSON.parse(buffer);
            if(data!=null){
                // route = data.routes[0];
                response.writeHead(200, {'Content-Type': 'application/json'});
                console.log("after write head")
                response.write(JSON.stringify(data))
                console.log("after write data")
            }
            response.end()
        });
    })
}

function printWeather(request,response){
    var url="http://api.openweathermap.org/data/2.5/forecast/daily?q=London,uk&mode=json&units=metric&cnt=16&appid=3cdfaaeba0a1a1f1a2b424b5cabc42a9"
    // var url='http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=3cdfaaeba0a1a1f1a2b424b5cabc42a9'
    var api_request = http.get(url, function (api_response) {
                    
        
        console.log("in api_request")
        // data is streamed in chunks from the server
        // so we have to handle the "data" event    
        var buffer = "", 
            weather_data,
            route;
    
        api_response.on("data", function (chunk) {
            buffer += chunk;
        }); 
        
        api_response.on("end", function (err) {
            // finished transferring data
            // dump the raw data
            console.log(buffer);
            console.log("\n");
            weather_data = JSON.parse(buffer);
            // route = data.routes[0];
    
            console.log("before write")
            response.writeHead(200, {'Content-Type': 'application/json'});
            console.log("after write head")
            response.write(JSON.stringify(weather_data))
            console.log("after write data")
            response.end()
        });
    })
}