var listApp=angular.module('listApp',[])
    
    var weathers=[]
    var eventList=[]
    listApp.controller('EventListController',function($scope,$http){
        var weather_url="/weather"
        var events_url="/events"
        // var currentDate=new Date()
        
        var ref = new Firebase("https://radiant-heat-6795.firebaseio.com/");
        var authData = ref.getAuth();
        if (authData) {
            $scope.loginLogoutUrl="/logout"
            $scope.btnLoginLogoutVal='Logout'
            console.log("User " + authData.uid + " is logged in with " + authData.provider);
        } else {
            $scope.loginLogoutUrl="/login"
            $scope.btnLoginLogoutVal='Login'
            // $('#nav-login-logout').click(function(){
            //   showLoginBox();  
            // })
          console.log("User is logged out");
        }
        
        $http.get(weather_url).success(function(data){
            var dataArray=Object.keys(data).map(function(k){return data[k]})
            weathers=dataArray[4]
            
            var event_weathers=[]
            
            $scope.weathers=weathers
        })
        
        $http.get(events_url).success(function(data) {
            var newdata=Object.keys(data).map(function(k){return data[k]})
            $.each(newdata,function(key,val){
              handleEventData(key,val)
            })
            $scope.events=eventList
        })
        
        
    })
    
    function handleEventData(key,obj){
        // var currentDate = new Date();
        // var event_date = new Date(obj.event_starting_date_time);
        // var timeDiff = Math.abs(event_date.getTime() - currentDate.getTime());
        // var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        
        // <ul class="no-bullet-list">
        //     <li>Event name:{{event.event_name}}</li>
        //     <li>Begin: {{event.event_starting_date_time}}</li>
        //     <li>Finish: {{event.event_ending_date_time}}</li>
        //     <li>Event venue: {{event.venue_name}}</li>
        //     <li>Description: {{event.event_description}}</li>
        // </ul>
        
        var currentDate = new Date();
        var event_date = new Date(obj.event_starting_date_time);
        var timeDiff = Math.abs(event_date.getTime() - currentDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            
            
        if(diffDays<16){
            var weather=weathers[diffDays]//$scope.thisCanBeusedInsideNgBindHtml = $sce.trustAsHtml(someHtmlVar);
            var tempMax='Maximum Temparature: '+weather.temp.max
            var tempMin='Minimum Temparature: '+weather.temp.min
            var tempDay='Day Temparature: '+weather.temp.day
            var tempNight='Night Temparature: '+weather.temp.night
            var weatherInfo='Weather: '+weather.weather[0].main+', '+weather.weather[0].description
        }else{
            var tempMax=''
            var tempMin=''
            var tempDay=""
            var tempNight=''
            var weatherInfo=''
        }
        
        console.log(tempMax)
        var eventObj={
            event_key:key,
            event_name:obj.event_name,
            event_description:obj.event_description,
            event_starting_date_time:obj.event_starting_date_time,
            event_ending_date_time:obj.event_ending_date_time,
            venue_name:obj.venue_name,
            temp_max:tempMax,
            temp_min:tempMin,
            temp_day:tempDay,
            temp_night:tempNight,
            weather_info:weatherInfo,
            weathers:weather
        }
        eventList.push(eventObj)
        // sessionStorage.setItem('event',eventObj)
    }