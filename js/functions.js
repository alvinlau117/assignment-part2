/**
 * Created by Lau Yat Hay on 26/3/2016.
 */

function changeMainContent(path,element){
        setListMenuActive(this)
        $('#main').hide()
        $('#main').load(path,function(){
                $(this).delay(450).show('slide',{direction:'right'})
        })
}

function setListMenuActive(listItem){
        clearListMenuActive(listItem)
        $(listItem).addClass('active')
        $(listItem).addClass({'pointer-events':'none'})
}

function clearListMenuActive(listItem){
        $(listItem).parent().children().removeClass('active')
}

function changeSidebarMenuById(menuId){
        $('#sidebar-menu').find('.list-group').hide('slide',{direction:'left'})
        $('#'+menuId).delay(450).show('slide',{direction:'left'})
}